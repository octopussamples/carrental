<?php 
// DB credentials.
define('DB_HOST','#{Azure.US.Primary.MySql.Name}.mysql.database.azure.com:3306');
//define('DB_USER','#{Azure.MySql.Admin.UserName}@#{Azure.US.Primary.MySql.Name}.mysql.database.azure.com');
define('DB_USER','#{Azure.MySql.Admin.UserName}');
define('DB_PASS','#{Azure.MySql.Admin.Password}');
define('DB_NAME','#{Project.Database.Name}');
// Establish database connection.
try
{
$dbh = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER, DB_PASS,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'",PDO::MYSQL_ATTR_SSL_CA => '/var/www/html/DigiCertGlobalRootG2.crt.pem',PDO::MYSQL_ATTR_SSL_VERIFY_SERVER_CERT => false));
}
catch (PDOException $e)
{
exit("Error: " . $e->getMessage());
}
?>